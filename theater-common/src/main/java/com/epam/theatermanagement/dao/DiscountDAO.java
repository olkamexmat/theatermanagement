package com.epam.theatermanagement.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.epam.theatermanagement.domain.User;

import javax.annotation.Resource;

@Repository
public class DiscountDAO {
    private static final String SQL_GET_COUNTER_ID__BY_STRATEGY =
            "SELECT DISCOUNT_COUNTER.COUNTER_ID FROM Counter INNER JOIN DISCOUNT_COUNTER " +
                    "ON Counter.COUNTER_ID = DISCOUNT_COUNTER.COUNTER_ID WHERE COUNTER_NAME = ?";
    private static final String SQL_UPDATE_STRATEGY_COUNTER =
            "UPDATE Discount_Counter SET USER_ID = ?, DISCOUNT_COUNTER_VALUE = ? WHERE COUNTER_ID = ?";
    private static final String SQL_GET_VALUE =
            "SELECT DISCOUNT_COUNTER_VALUE FROM Counter INNER JOIN DISCOUNT_COUNTER " +
                    "ON Counter.COUNTER_ID = DISCOUNT_COUNTER.COUNTER_ID WHERE COUNTER_NAME = ?";
    
    @Resource(name = "getJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    private int getIdCounter(String name) {
        return jdbcTemplate.queryForObject(
                SQL_GET_COUNTER_ID__BY_STRATEGY, new Object[] {name}, Integer.class
        );
    }

    /**
     * Updating counter value for certain user and strategy which worked
     * @param nameStrategy the name of discount strategy which gives total discount
     *                     @see com.epam.theatermanagement.discount
     * @param user the user for who count discount
     * @param value how many times <code>nameStrategy</code> gives total discount for <code>user</code>
     */
    public void update(String nameStrategy, User user, int value) {
        jdbcTemplate.update(
                SQL_UPDATE_STRATEGY_COUNTER,
                user.getId(), value, getIdCounter(nameStrategy)
        );
    }
    
    /**
     * Getting current counter value from table
     * @param name the name of executed method
     * @return current value of counter
     */
    public int getValue(String name) {
        return jdbcTemplate.queryForObject(
                SQL_GET_VALUE, new Object[] {name}, Integer.class
        );
    }
}
