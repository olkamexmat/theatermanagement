package com.epam.theatermanagement.config;


import com.epam.theatermanagement.domain.Auditorium;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Configuration
@ImportResource("classpath:theater-spring.xml")
@ComponentScan({"com.epam.theatermanagement"})
@PropertySource({"classpath:auditorium.properties"})
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class ApplicationConfig {
    @Autowired
    DataSource dataSource;

    @Bean
    public JdbcTemplate getJdbcTemplate() {
        return new JdbcTemplate(dataSource);
    }

    @Autowired
	private Environment environment;

	public Auditorium getAuditorium(String name, long numberOfSeats, Set<Long> vipSeats) {
		Auditorium auditorium = new Auditorium();
		auditorium.setName(name);
		auditorium.setNumberOfSeats(numberOfSeats);
		auditorium.setVipSeats(vipSeats);
		return auditorium;
	}

	@Bean
	public Set<Auditorium> getAuditoriumSet() {
		Set<Auditorium> auditoriums = new HashSet<>();

		String nameFirstAuditorium = environment.getProperty("firstAud.name");
		long numberSeatsFirstAuditorium = Long.valueOf(environment.getProperty("firstAud.numberOfSeats"));
		Set<Long> vipSeatsFirstAuditorium = Pattern.compile(",").splitAsStream(environment.getProperty("firstAud.vipSeats"))
				.map(Long::parseLong).collect(Collectors.toSet());

		String nameSecondAuditorium = environment.getProperty("secondAud.name");
		long numberSeatsSecondAuditorium = Long.valueOf(environment.getProperty("secondAud.numberOfSeats"));
		Set<Long> vipSeatsSecondAuditorium = Pattern.compile(",").splitAsStream(environment.getProperty("secondAud.vipSeats"))
				.map(Long::parseLong).collect(Collectors.toSet());
		
		auditoriums.add(getAuditorium(nameFirstAuditorium, numberSeatsFirstAuditorium, vipSeatsFirstAuditorium));
		auditoriums.add(getAuditorium(nameSecondAuditorium, numberSeatsSecondAuditorium, vipSeatsSecondAuditorium));
		
		return auditoriums;
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}
