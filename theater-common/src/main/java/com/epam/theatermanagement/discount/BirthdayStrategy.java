package com.epam.theatermanagement.discount;

import org.springframework.stereotype.Service;

import com.epam.theatermanagement.domain.Event;
import com.epam.theatermanagement.domain.User;

import java.time.LocalDate;

import static java.lang.Math.abs;

/**
 * @author Volha_Shautsova
 */
@Service
public class BirthdayStrategy implements DiscountStrategy {
    private static final int DAY_LIMITED = 5;
    private static final int FIVE_PERCENT = 5;

    public BirthdayStrategy() {

    }

    /**
     * Executing birthday strategy
     * If user birthday within 5 days of <code>airDateTime</code>
     *      then user gets 5% discount
     *      else nothing
     *
     * @param user for which execute strategy
     * @param event for which buy ticket
     * @param airDateTime on which event taking place
     * @param numberOfTickets amount of booked ticket
     * @return discount 5% if birthday of user within 5 days of air date else 0
     */
    public int executeStrategy(User user, Event event, LocalDate airDateTime, long numberOfTickets) {
        if (user != null && (abs(airDateTime.getDayOfMonth() - user.getBirthday().getDayOfMonth()) <= DAY_LIMITED)) {
            return FIVE_PERCENT;
        }
        return 0;
    }
}
