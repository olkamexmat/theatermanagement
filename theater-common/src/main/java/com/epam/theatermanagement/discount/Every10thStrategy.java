package com.epam.theatermanagement.discount;

import org.springframework.stereotype.Service;

import com.epam.theatermanagement.domain.Event;
import com.epam.theatermanagement.domain.User;

import java.time.LocalDate;

/**
 * @author Volha_Shautsova
 */
@Service
public class Every10thStrategy implements DiscountStrategy {
    private static final int FIFTY_PERCENT = 50;
    private static final int GET_TENTH = 10;

    public Every10thStrategy() {

    }

    /**
     * Giving 50% discount for each tenth booked ticket
     *
     * @param user for which execute strategy
     * @param event for which buy ticket
     * @param airDateTime on which event taking place
     * @param numberOfTickets amount of booked ticket
     * @return discount 50% for each tenth booked ticket
     */
    public int executeStrategy(User user, Event event, LocalDate airDateTime, long numberOfTickets) {
        long commonNumberOfTickets = user.getTickets().size() + numberOfTickets;
        long amount = commonNumberOfTickets / GET_TENTH;
        return (int) (FIFTY_PERCENT * amount);
    }
}
