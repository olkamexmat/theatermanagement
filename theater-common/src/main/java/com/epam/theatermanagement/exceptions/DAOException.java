package com.epam.theatermanagement.exceptions;


public class DAOException extends RuntimeException {
	/**
	 * generated serial version
	 */
	private static final long serialVersionUID = -3338068223333784439L;

	
	public DAOException() {
	}

	public DAOException(String message, Throwable exception) {
		super(message, exception);
	}

	public DAOException(String message) {
		super(message);
	}

	public DAOException(Throwable exception) {
		super(exception);
	}
}

