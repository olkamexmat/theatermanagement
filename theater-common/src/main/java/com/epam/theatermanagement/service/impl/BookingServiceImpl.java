package com.epam.theatermanagement.service.impl;

import com.epam.theatermanagement.dao.TicketDAO;
import com.epam.theatermanagement.domain.*;
import com.epam.theatermanagement.service.BookingService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;

import java.time.LocalDate;
import java.util.Set;

/**
 * @author Volha_Shautsova
 */
public class BookingServiceImpl extends AbstractDomainServiceImpl<Ticket> implements BookingService {
    private static final Logger LOGGER = LogManager.getLogger(BookingServiceImpl.class);
    private static final double RATING_MULTIPLIER = 1.2;
    private static final double VIP_SEATS_MULTIPLIER = 2;
    @Autowired
    private TicketDAO ticketDAO;

    @Autowired
    private DiscountServiceImpl discountServiceImpl;

    @Override
    public double getTicketsPrice(@Nonnull Event event, @Nonnull LocalDate dateTime, User user, @Nonnull Set<Long> seats) {
        double discount = discountServiceImpl.getDiscount(user, event, dateTime, user.getTickets().size());
        double price = event.getBasePrice();
        double finishPrice = 0;

        LOGGER.debug(price);

        if (event.getRating() == EventRating.HIGH) {
            price *= RATING_MULTIPLIER;
        }

        Auditorium auditorium = event.getAuditoriums().get(dateTime);
        Set<Long> vipSeatsInAuditorium = auditorium.getVipSeats();

        for (Long bookedSeat : seats) {
            if (vipSeatsInAuditorium.contains(bookedSeat)) {
                finishPrice += VIP_SEATS_MULTIPLIER * price;
            } else {
                finishPrice += price;
            }
        }

        return finishPrice * discount;
    }

    @Override
    public void bookTickets(@Nonnull Set<Ticket> tickets) {
        tickets.forEach(ticketDAO::save);
    }

    @Nonnull
    @Override
    public Set<Ticket> getPurchasedTicketsForEvent(@Nonnull Event event, @Nonnull LocalDate dateTime) {
        return ticketDAO.getPurchasedTicketsForEvent(event, dateTime);
    }

    public void setDiscountServiceImpl(DiscountServiceImpl discountServiceImpl) {
        this.discountServiceImpl = discountServiceImpl;
    }

    public TicketDAO getTicketDAO() {
        return ticketDAO;
    }

    public void setTicketDAO(TicketDAO ticketDAO) {
        this.ticketDAO = ticketDAO;
    }
}
