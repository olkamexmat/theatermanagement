package com.epam.theatermanagement.service.impl;

import com.epam.theatermanagement.dao.AirDateDAO;
import com.epam.theatermanagement.dao.EventDAO;
import com.epam.theatermanagement.domain.Event;
import com.epam.theatermanagement.service.EventService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;

import java.time.LocalDate;
import java.util.Collection;

/**
 * @author Volha_Shautsova
 */
@Service
public class EventServiceImpl extends AbstractDomainServiceImpl<Event> implements EventService {
    @Autowired
    private EventDAO eventDAO;
    @Autowired
    private AirDateDAO airDateDAO;

    @Override
    public Event save(@Nonnull Event event) {
        return eventDAO.save(event);
    }

    @Override
    public void remove(@Nonnull Event event) {
        eventDAO.remove(event);
    }

    @Override
    public Event getById(@Nonnull Long id) {
        return eventDAO.getById(id);
    }

    @Nonnull
    @Override
    public Collection<Event> getAll() {
        return eventDAO.getAll();
    }

    @Override
    public Event getByName(@Nonnull String name) {
        return eventDAO.getByName(name);
    }
    
    public Collection<LocalDate> getAllDates() {
    	return airDateDAO.getAllDates();
    }
    
    public void update(@Nonnull Long id, @Nonnull Event event) {
    	eventDAO.update(id, event);
    }
}
