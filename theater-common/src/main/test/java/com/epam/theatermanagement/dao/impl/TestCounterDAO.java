package com.epam.theatermanagement.dao.impl;

import com.epam.theatermanagement.dao.CounterDAO;
import com.epam.theatermanagement.dao.EventDAO;
import com.epam.theatermanagement.domain.Event;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:AppContextTest.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:dataset.xml")
@DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class TestCounterDAO {
	@Autowired
	private CounterDAO counterDAO;
	@Autowired
	private EventDAO eventDAO;

	@Test
	public void updateTest() {
		Event event = eventDAO.getById((long) 1);
		final int value = 1;
		final String name = "accessByName";
		counterDAO.update(event, name, value);
		assertTrue(counterDAO.getValue(name) == value);
	}
	
	@Test
	public void getValueTest() {
		assertNotNull(counterDAO.getValue("accessByName"));
	}
}
