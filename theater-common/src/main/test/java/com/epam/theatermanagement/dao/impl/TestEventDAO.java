package com.epam.theatermanagement.dao.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.theatermanagement.dao.EventDAO;
import com.epam.theatermanagement.domain.Event;
import com.epam.theatermanagement.domain.EventRating;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.NavigableSet;
import java.util.TreeSet;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:AppContextTest.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:dataset.xml")
@DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class TestEventDAO {
	@Autowired
	private EventDAO eventDAO;
	
	@Test
	public void getByNameTest() {
		String name = "grand concert";
		assertNotNull(eventDAO.getByName(name));
	}
	
	@Test
	public void saveTest() {
		NavigableSet<LocalDate> airDates = new TreeSet<>();
		airDates.add(LocalDate.of(2016, 6, 10));
		airDates.add(LocalDate.of(2016, 5, 28));
		
		Event event = new Event();
		event.setName("Bi-2 concert");
		event.setBasePrice(14);
		event.setRating(EventRating.HIGH);
		event.setAirDates(airDates);
		
		event = eventDAO.save(event);
		Event expectedEvent = eventDAO.getById(event.getId());
		assertTrue(event.equals(expectedEvent));
	}
	
	@Test(expected = EmptyResultDataAccessException.class)
	public void removeTest() {
		Event event = eventDAO.getById((long) 21);
		eventDAO.remove(event);
		assertNull(eventDAO.getById((long) 21));
	}
	
	@Test
	public void getById() {
		long id = 1;
		assertNotNull(eventDAO.getById(id));
	}
	
	@Test 
	public void getAllTest() {
		assertNotNull(eventDAO.getAll());
	}
}
