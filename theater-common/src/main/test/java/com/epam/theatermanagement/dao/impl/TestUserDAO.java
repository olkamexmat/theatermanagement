package com.epam.theatermanagement.dao.impl;

import com.epam.theatermanagement.dao.UserDAO;
import com.epam.theatermanagement.domain.User;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.SQLException;
import java.time.LocalDate;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:AppContextTest.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:dataset.xml")
@DatabaseTearDown(value = "classpath:dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class TestUserDAO {
	@Autowired
	private UserDAO userDAO;
	
	@Test(expected = DuplicateKeyException.class)
	public void saveTest() {
		User user = new User();
		user.setFirstName("Ivan");
		user.setLastName("Kit");
		user.setEmail("kit_ivan@mail.ru");
		user.setBirthday(LocalDate.of(1975, 8, 5));
		user = userDAO.save(user);
		User userExpected = userDAO.getById(user.getId());
		assertTrue(user.equals(userExpected));
	}
	
	@Test(expected = EmptyResultDataAccessException.class)
	public void removeTest() {
		User user = userDAO.getById((long) 48);
		userDAO.remove(user);
		assertNull(userDAO.getById((long) 48));
	}
	
	@Test
	public void getByIdTest() {
		long id = 49;
		assertNotNull(userDAO.getById(id));
	}
	
	@Test
	public void getUserByEmailTest() throws SQLException {
		String email = "vasiliy@gmail.com";
		assertNotNull(userDAO.getUserByEmail(email));
	}
	
	@Test
	public void getAllTest() {
		assertNotNull(userDAO.getAll());
	}
}
