<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<h2>All events</h2>
<table>
	<thead>
		<tr>
			<th>Name</th>
			<th>Price</th>
			<th>Air dates</th>
			<th></th>
		</tr>
	</thead>
	<c:forEach var="event" items="${eventsList}">
		<tr>
			<td>${event.name}</td>
			<td>${event.basePrice}$</td>
			<td><c:choose>
					<c:when test="${not empty event.airDates}">
						<c:forEach var="airDate" items="${event.airDates}"
							varStatus="loop">
							${airDate}<c:if test="${not loop.last}">,</c:if>
						</c:forEach>
					</c:when>
					<c:otherwise>
							not defined
					</c:otherwise>
				</c:choose></td>

			<td>
				<spring:url value="/events/edit/${event.id}" var="eventEditUrl" /> 
				<spring:url value="/events/delete/${event.id}" var="eventDeleteUrl" />
					
				<button onclick="location.href='${eventEditUrl}'">Edit</button>
				<form method="POST" action="${eventDeleteUrl}">
					<input type="submit" value="Delete"/>
				</form>
			</td>
		</tr>
	</c:forEach>
</table>

<spring:url value="/events/add" var="eventAddUrl" />
<button onclick="location.href='${eventAddUrl}'">Add event</button>
