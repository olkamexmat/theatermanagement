<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<head>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

<!-- Script for calendar -->
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<%-- <spring:url value="/resources/js/jquery.1.10.2.min.js" var="jqueryJs" />
<script src="${jqueryJs}"></script> --%>

<script>
	$(function() {
		$("#datepicker").datepicker();
	});
</script>

<script type="text/javascript">

	function addViaAjax() {
		var header = $("meta[name='_csrf_header']").attr("content");
		var token = $("meta[name='_csrf']").attr("content");

		var forAdd = {};

		forAdd["name"] = $("#eventName").val();
		forAdd["basePrice"] = parseFloat($("#basePrice").val());
		forAdd["airDates"] = $("#datepicker").val();
		forAdd["rating"] = $("#rating").val();
		
		forAdd["auditoriumName"] = $("#auditoriumName").val();
		
		/* for console logging*/
		console.log(forAdd);

		$.ajax({
			type : "POST",
			contentType : "application/json, charset=utf-8",
			url : "/theater-admin/events",
			data : JSON.stringify(forAdd),
			dataType : 'json',
			timeout : 100000,
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json'
			},
			success : function(data) {
				console.log("SUCCESS: ", data);
				display(data);
			},
			error : function(e) {
				console.log("ERROR: ", e);
				display(e);
			}
		});

	}

	function display(data) {
		var json = "<h4>Ajax Response</h4><pre>"
				+ JSON.stringify(data, null, 5) + "</pre>";
		$('#feedback').html(json);
	}
</script>
</head>

<c:set var="localeCode" value="${pageContext.response.locale}" />
<script>
	var localeCode = '${localeCode}';
</script>

<div id="feedback"></div>

<div>
	<h2>Add</h2>

	<div>
		<label>Name</label>
		<div>
			<input type="text" id="eventName" placeholder="Name" />
		</div>
	</div>

	<div>
		<label>Base price</label>
		<div>
			<input id="basePrice" placeholder="basePrice" />
		</div>
	</div>

	<div>
		<label>Choose rating</label> <select id="rating">
			<option value="LOW">LOW</option>
			<option value="MID">MID</option>
			<option value="HIGH">HIGH</option>
		</select>
	</div>

	<div>		
		Date: <input type="text" id="datepicker">
	</div>

	<div>
		<label>Choose auditorium</label>
		<select id="auditoriumName">
			<option value="Queen">Queen</option>
		</select>
	</div>
	
	<div>
		<div>
			<button type="button" onclick="addViaAjax()">Add event</button>
		</div>
	</div>
	<div>
		<a href="${pageContext.request.contextPath}">Back to List</a>
	</div>
</div>
