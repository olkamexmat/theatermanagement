<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div>
	<h2>Edit</h2>

	<spring:url value="/events" var="eventActionUrl" />

	<form:form method="post" modelAttribute="eventForm"
		action="${eventActionUrl}">

		<form:hidden path="id" />

		<spring:bind path="name">
			<div>
				<label>Name</label>
				<div>
					<form:input path="name" type="text" id="name" placeholder="${eventForm.name}" />
					<form:errors path="name" />
				</div>
			</div>
		</spring:bind>

		<spring:bind path="basePrice">
			<div>
				<label>Base price</label>
				<div>
					<form:input path="basePrice" id="basePrice" placeholder="${eventForm.basePrice}" />
					<form:errors path="basePrice" />
				</div>
			</div>
		</spring:bind>

		<spring:bind path="rating">
			<div>
				<label>Choose rating</label>
				<form:select path="rating">
					<form:option value="LOW">LOW</form:option>
					<form:option value="MID">MID</form:option>
					<form:option value="HIGH">HIGH</form:option>
				</form:select>
			</div>
		</spring:bind>

		<div>
			<div>
				<button type="submit">Save</button>
			</div>
		</div>
	</form:form>

	<div>
		<a href="/theater-admin">Back to List</a>
	</div>
</div>