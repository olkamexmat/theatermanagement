package com.epam.theatermanagement.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.NavigableSet;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.epam.theatermanagement.domain.Event;
import com.epam.theatermanagement.domain.EventRating;
import com.epam.theatermanagement.exceptions.ServiceException;
import com.epam.theatermanagement.service.impl.AuditoriumServiceImpl;
import com.epam.theatermanagement.service.impl.EventServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class EventController {
	private final Logger LOGGER = LoggerFactory.getLogger(EventController.class);

	@Autowired
	private EventServiceImpl eventService;
	
	@Autowired
	private AuditoriumServiceImpl auditoriumService;

	private ObjectMapper objectMapper = new ObjectMapper();

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model) {
		LOGGER.debug("index()");
		return "redirect:/events";
	}

	@RequestMapping(value = "/events", method = RequestMethod.GET)
	public String showAllEvents(Model model) {
		model.addAttribute("eventsList", eventService.getAll());
		return "events";
	}

	@RequestMapping(value = "/events", method = RequestMethod.POST)
	public @ResponseBody String saveOrUpdateEvent(@RequestBody String eventString,
			@ModelAttribute("eventForm") Event event) throws ServiceException, JsonProcessingException, IOException {
		if (event.isNew()) {
			JsonNode jsonNode = objectMapper.readTree(eventString);
			Event eventToAdd = new Event();
			String name = objectMapper.convertValue(jsonNode.get("name"), String.class);
			Double basePrice = objectMapper.convertValue(jsonNode.get("basePrice"), Double.class);
			
			NavigableSet<LocalDate> airDates = new TreeSet<>();
			String date = objectMapper.convertValue(jsonNode.get("airDates"), String.class);
			final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/d/yyyy");			
			airDates.add(LocalDate.parse(date, dateTimeFormatter));
			
			EventRating rating = EventRating.valueOf(objectMapper.convertValue(jsonNode.get("rating"), String.class));
			String auditoriumName = objectMapper.convertValue(jsonNode.get("auditoriumName"), String.class);
			
			// NavigableMap<LocalDate, Auditorium> auditoriums = null;
			
			eventToAdd.setAirDates(airDates);
			// event.setAuditoriums(auditoriums);
			eventToAdd.setBasePrice(basePrice);
			eventToAdd.setName(name);
			eventToAdd.setRating(rating);
			
			eventService.save(eventToAdd);
			auditoriumService.assignAuditorium(eventToAdd, auditoriumName);
		} else {
			eventService.update(event.getId(), event);
		}

		return eventString;
	}

	@RequestMapping(value = "/events/add", method = RequestMethod.GET)
	public String showAddEventForm(Model model) {
		model.addAttribute("eventForm", new Event());
		model.addAttribute("airDates", eventService.getAllDates());
		return "event_add";
	}

	@RequestMapping(value = "/events/edit/{id}", method = RequestMethod.GET)
	public String editEvent(Model model, @PathVariable("id") Long eventId, @ModelAttribute Event event) {
		model.addAttribute("eventForm", event);
		return "event_edit";
	}

	@RequestMapping(value = "/events/delete/{id}", method = RequestMethod.POST)
	public String deleteEvent(@PathVariable Long id, @ModelAttribute Event event) {
		eventService.remove(eventService.getById(id));
		return "redirect:/events/";
	}

}
